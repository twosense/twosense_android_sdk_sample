package ai.twosense.sample;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ai.twosense.sdk.*;

public class MainActivity extends AppCompatActivity {

    protected static final int PERMISSION_LOCATION_REQUEST_CODE = 23457;

    protected ITwoSenseSDK twoSenseSDK;

    protected boolean demoSessionStarted = false;

    protected Button toggleTwoSenseSDKButton;
    protected TextView userIdTextView;
    protected Button toggleDemoSessionButton;
    protected TextView authenticateResultTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Instantiate the SDK singleton instance
        twoSenseSDK = TwoSenseSDK.getInstance(getApplicationContext());

        userIdTextView = (TextView) findViewById(R.id.main_content_user_id);

        toggleTwoSenseSDKButton = (Button) findViewById(R.id.main_start_stop);
        toggleTwoSenseSDKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleTwoSenseSDK();
            }
        });

        toggleDemoSessionButton = (Button) findViewById(R.id.main_authenticate);
        toggleDemoSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleDemoSession();
            }
        });
        authenticateResultTextView = (TextView) findViewById(R.id.main_content_authentication);
        initTwoSenseSDK();
    }

    @Override
    protected void onStart() {
        super.onStart();
        showPermissionDialogIfNoLocationPermission();
    }

    protected void initTwoSenseSDK() {
        updateUI();
    }

    protected void updateUI() {
        //check if the SDK is running, i.e. .start() has been called
        if (twoSenseSDK.isRunning()) {
            toggleTwoSenseSDKButton.setText(getString(R.string.main_stop_demo_label));
        } else {
            toggleTwoSenseSDKButton.setText(getString(ai.twosense.sample.R.string.main_start_demo_label));
        }
        //Display the SDK's user ID
        userIdTextView.setText(getString(ai.twosense.sample.R.string.main_content_user_id) + " "
                + twoSenseSDK.getUserId());
    }

    protected void toggleTwoSenseSDK() {
        //check if the SDK is running
        if (twoSenseSDK.isRunning()) {
            //stop the SDK
            twoSenseSDK.stop();
            updateUI();
        } else {
            showUserIdInput();
        }
    }

    protected void toggleDemoSession() {
        //check if the SDK is running
        if (twoSenseSDK.isRunning()) {
            //check if the training period is complete
            if (twoSenseSDK.getTrainingProgress() == 1f) {
                //check if a demo has been started and is running
                if (demoSessionStarted) {
                    //Complete the demo session and get back a score
                    ITwoSenseSDK.IAuthenticationResult authResult = twoSenseSDK.endDemoSession();
                    if (authResult != null) {
                        //getConfidence() returns the score
                        authenticateResultTextView.setText(getString(R.string.main_content_authenticate) + " " + authResult.getConfidence());
                    } else {
                        authenticateResultTextView.setText("Error.");
                    }
                    toggleDemoSessionButton.setText(getString(R.string.main_start_demo));
                    demoSessionStarted = false;
                } else {
                    try {
                        //start a demo session
                        twoSenseSDK.beginDemoSession();
                        demoSessionStarted = true;
                        toggleDemoSessionButton.setText(getString(R.string.main_stop_demo));
                    } catch (IllegalStateException e) {
                        Toast.makeText(getApplicationContext(), "Demo could not be started.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    protected void showUserIdInput() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.twosense_user_id_dialog_title);
        final EditText input = new EditText(this);

        //get the user ID if it's been set
        input.setText(twoSenseSDK.getUserId());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                updateUserIdAndStart(input.getText().toString());
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    protected void updateUserIdAndStart(String userId) {
        try {
            //set the user ID for this instance
            twoSenseSDK.setUserId(userId);
            //start the demo instance (it wil run in the background)
            twoSenseSDK.start();
            updateUI();
        } catch (Exception e) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.activity_main_layout), "Exception: "
                    + e.getMessage(), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    protected void showPermissionDialogIfNoLocationPermission() {
        if (!checkLocationPermission(this)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_LOCATION_REQUEST_CODE);
        }
    }

    public static boolean checkLocationPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
}
