# TwoSense SDK Sample for Android
Copyright (C) 2017 TwoSense, Inc.

This is the developer guide for using the TwoSense Android Demo SDK.

To add the TwoSenseSDK library to your project, follow these steps:

- Open `gradle.properties` file in the top-level directory of the app project.
- Add the following lines to `gradle.properties`:
```Java
	artifactory_username=[will be sent via email]
	artifactory_password=[will be sent via email]
```
- Open __**project-level**__ `build.gradle` file and add the `maven{...}` part of the code like below:

```Java
allprojects {
    repositories {
        jcenter()
        // add these lines
        maven {
            url "http://maven.twosense.ai/artifactory/libs-release-local"
            credentials {
                username = "${artifactory_username}"
                password = "${artifactory_password}"
            }
        }
    }
}
```

- Open build.gradle inside the module that you want to use TwoSenseSDK in, and simply add a dependency:

```Java
dependencies {
    compile 'ai.twosense:twosense-android-sdk:0.7.2'
}
```
 
The SDK consists of a library containing the interface `ITwoSenseSDK` and the implementation `TwoSenseSDK.java`.
 
 The interface is documented as follows:

```Java
public interface ITwoSenseSDK {


    /**
     *
     * Gets a string describing the version.
     *
     * @return the version name
     */
    String getSDKVersionName();

    /**
     *
     * Gets a number indicating the version.
     *
     * @return the version number.
     */
    int getSDKVersionNumber();

    /**
     * Assigns a string that is used as the unique ID of this user.
     *
     * @param userID the ID that will identify this user.
     * @throws IllegalArgumentException if the string is empty, is less than 10 characters, or contains illegal characters.
     */
    void setUserId(String userID) throws IllegalArgumentException;

    /**
     * Get the previously set user ID string.
     *
     * @return user ID string, null if it has not been set previously
     */
    String getUserId();

    /**
     * Starts the behavioral data collection, processing and training service in the background.
     * This only needs to be called once, and persists across updates and device restarts.
     *
     * @throws IllegalStateException is the ID has not been set for this instance.
     */
    void start() throws IllegalStateException;


    /**
     *
     * Gets the status of the SDK.
     *
     * @return true if the SDK is running (start() has been called), false otherwise.
     */
    boolean isRunning();


    /**
     * Queries the system for readiness level of the behavioral authenticator.
     *
     * @return float in range (0, 1) where < 1 is not yet ready and 1 is ready.  0 will be returned if no model is present.
     * @throws IllegalStateException if the service has not been started.
     */
    float getTrainingProgress() throws IllegalStateException;

    /**
     * Begins a demonstration event.
     *
     * @throws IllegalStateException if the process is not running, i.e. the start() command has not been called previously.
     */
    void beginDemoSession() throws IllegalStateException;

    /**
     * Completes a demonstration event and returns the authentication result.
     * This can only be called after beginDemoSession() has been called previously.
     *
     * @return the authentication status of the current user. If the behavioral model is not present on the device,
     *      (i.e. training is not yet complete, or the model has not yet been downloaded) 
     *      the authentication status returned will be "UNKNOWN" with a confidence of -1.
     * @throws IllegalStateException if beginDemoSession() has not been called previously.
     */
    IAuthenticationResult endDemoSession() throws IllegalStateException;

    /**
     * Returns an instantaneous indicator of the authentication state of the current user.
     *
     * NOTE: THIS IS NOT FOR USE IN THE DEMO.  The demo is session based.
     * authenticate() is configured to match customer requirements in a product setting.
     *
     * @return the authentication status of the current user.
     * @throws IllegalStateException if TwoSenseSDK has not been started.
     */
    IAuthenticationResult authenticate() throws IllegalStateException;

    /**
     * Stops the data collection, training and authentication process.
     * This should only be used if the usage of TwoSense's behavioral biometrics are no longer needed in the future on this device.
     */
    void stop();

    /**
     * Interface to the object that contains the result of a demo / authentication event.
     */
    public interface IAuthenticationResult{

        /**
         * Gets a recommendation based on the confidence for this result.
         * @return an enum that indicates the recommentation.
         */
        public AuthRecommendation isAuthenticated();

        /**
         * Gets the confidence of this decision, 0 being absolute confidence that this is not the
         *      authorized user, 100 being absolute confidence that this is the authorized user.
         * @return the confidence, between 0 and 100.
         */
        public int getConfidence();

        /**
         * Gets the time (Date) at which the authentication event occurred.
         * @return the Date of the auth event.
         */
        public Date getTimestamp();

        /**
         * Gets the approximate location at which the authentication event occurred.
         * @return auth event location.
         */
        public LatLng getLocation();

    }

    /**
     * An enum that models recommendation states based on this authentication event.
     */
    public enum AuthRecommendation {
        AUTHENTICATED, UNKNOWN, NOT_AUTHENTICATED;
    }


}
```

Using the SDK can be achieved as follows.
First, the SDK instance must be initialized: 

```Java
//Get an instance of the service
TwoSenseSDK twoSenseSDK = TwoSenseSDK.getInstance(this.getApplicationContext());
```

Next, the SDK needs to receive a unique hash that is used to identify this user for all future communications.
This ID is persistent, and must only be set once.
     
```Java
//Set the user ID
//need only be done once per install
twoSenseSDK.setUserID("TestUserID1");
```
     
Next, the `.start()` command deploys the SDK to collect behavioral data and to begin training models of the user's behavioral signature.

```Java
//Start the service
//need only be done once per install
twoSenseSDK.start()
```
     
The progress of the training process can be checked periodically.
Progress will be < 1 until it is complete:

```Java
 //Get the progress of training
 float progress = twoSenseSDK.getTrainingProgress();
```
     
Before training is complete, all authentication queries will return `AuthRecommendation.UNKNOWN` with a score of 50.
Once progress is complete, a demo session can be started:

```Java
// Begin a demo session
twoSenseSDK.beginDemoSession()
```

After the demo behavior has been performed, the demo session can be ended and the SDK will return a score.
That score will indicate whether or not the SDK believes that behavior was performed by the authorized user (> 90), or not.

```Java
// Stop the demo and get the result
IAuthenticationResult result = twoSenseSDK.endDemoSession();
```
 
When not in demo mode, the `.authenticate()` command can be used at any time to obtain a score for the identity of the user.
 
```Java
// Get an authentication result at any time, foreground or background (not in demo).
IAuthenticationResult result = twoSenseSDK.authenticate();
```

To disable the SDK, e.g. if the user manually logs out, or use is no longer needed, the SDK should be stopped:

```Java
// Stop the SDK
twoSenseSDK.stop();
```

In the rare event that the ID must be changed, stop the service, change the ID and restart (do no hot-swap):

```Java
// Stop the SDK
twoSenseSDK.stop();
// Change the user ID
twoSenseSDK.setUserID("TestUserID2");
// Restart the service
twoSenseSDK.start();
```

An application that utilizes the TwoSense SDK must also extend the `TwoSenseApplication` class:

```Java
public class SampleApplication extends TwoSenseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}

```

The `SampleApplication` class, which now is a subclass of `TwoSenseApplication`, must be added to the `AndroidManifest.xml` file
under the `application` tag:

```XML
    <application
        android:name=".SampleApplication"
        ...
    </application>
```
 
 


